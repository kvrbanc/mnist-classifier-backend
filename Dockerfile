# Base image - Python 3.10.10
FROM bitnami/python:3.10.10

# Create a new working directory and copy application files within it
WORKDIR /usr/src/backend
COPY app /usr/src/backend/app
COPY requirements.txt /usr/src/backend/requirements.txt
COPY .env /usr/src/backend/.env

# Create folder to store logs
RUN mkdir logs

# Install necessary python packages from the 'requirements.txt' file
RUN pip3 install --upgrade pip
RUN python3 -m pip install --no-cache-dir --no-input -r requirements.txt && rm requirements.txt

# Set default shell
ENV SHELL /usr/bin/bash

# Port for accessing API server
EXPOSE 8006

# Default shell is BASH
ENV SHELL /bin/bash

# Add docker-compose-wait tool
ENV WAIT_VERSION 2.7.2
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/$WAIT_VERSION/wait /wait
RUN chmod +x /wait

# Run the uvicorn server
CMD "uvicorn" "app.main:app" "--host" "0.0.0.0" "--port" "8006"