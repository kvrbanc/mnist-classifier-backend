# MNIST-classifier-backend

This repository contains code that implements the backend part of the MNIST-classifier system. The system consists of Frontend, Backend, and Classifier service. Git repositories of the remaining parts of the system:
- Frontend: https://gitlab.com/kvrbanc/mnist-classifier-frontend
- Classifier service: https://gitlab.com/kvrbanc/mnist-classifier

## General notes

The **documentation** of the API endpoints is available at URL: http://localhost:8006/docs

To **verify** users and **reset user passwords**, *emails* are sent. The email are sent using a **Gmail** account.

New users are created by providing an *email*, a *password*, and a *retyped password*. Contstraints on the credentials:
- *Email* has to be a **valid email address**
- *Password* and *retyped password* need to be at least **8 characters long**