from pydantic import BaseSettings, EmailStr


class Settings(BaseSettings):
    """
    A class that loads and validates environment variables
    """
    DATABASE_URL: str
    MONGO_INITDB_DATABASE: str

    JWT_PUBLIC_KEY: str
    JWT_PRIVATE_KEY: str
    ACCESS_TOKEN_EXPIRES_IN: int
    JWT_ALGORITHM: str

    CLIENT_ORIGIN_CONTAINER: str
    CLIENT_ORIGIN_LOCALHOST: str
    EMAIL_LINK_CLIENT_ORIGIN: str

    EMAIL_HOST: str
    EMAIL_PORT: int
    EMAIL_USERNAME: str
    EMAIL_PASSWORD: str
    EMAIL_FROM: EmailStr

    CLASSIFIER_URL: str

    class Config:
        env_file = './.env'



settings = Settings()