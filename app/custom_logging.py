import logging
from pathlib import Path
import sys


def get_logger(
    *,
    logger_name : str,
    logging_file: str
) -> logging.Logger:
    """
    Creates a logger that logs information to stderr and a file

    Args:
        logger_name: The name of the logger
        logging_file: The path to a file to which to store logs to

    Returns:
        logging.Logger: A logger instance
    """

    # Configure the 'root' logger
    logging.basicConfig(
        level=logging.INFO,
        format='%(levelname)s: [%(asctime)s](%(name)s)  %(message)s'
    )

    # Create a custom logger that writes into the file
    logger = logging.getLogger(logger_name)

    # Create handler
    file_handler = logging.FileHandler(logging_file, mode="a")
    file_handler.setLevel(logging.WARNING)

    # Create formatter and add it to handler
    format = logging.Formatter('%(levelname)s: [%(asctime)s](%(name)s)  %(message)s')
    file_handler.setFormatter(format)

    # Add handler to the logger
    logger.addHandler(file_handler)

    return logger


# Define log file path
root_path = Path(__file__).parent.parent
log_path = root_path / "logs" / "logfile.log"