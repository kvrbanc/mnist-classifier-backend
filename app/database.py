from pymongo import mongo_client
import pymongo
from app.config import settings
from app.custom_logging import get_logger, log_path
import logging

# Load logger
logger = get_logger(
    logger_name=__name__,
    logging_file=log_path
)

# Intantiate Mongo client
client = mongo_client.MongoClient(
    settings.DATABASE_URL, 
    serverSelectionTimeoutMS=5000
)

# Connect to the database
try:
    conn = client.server_info()
    logger.info(f'Connected to MongoDB {conn.get("version")}')
except Exception:
    logger.exception("Unable to connect to the MongoDB server.")


db = client[settings.MONGO_INITDB_DATABASE]
# Get/Create a 'Users' collection
User = db.users
User.create_index([("email", pymongo.ASCENDING)], unique=True) # Unique contraint on the email field