from typing import List
from fastapi_mail import FastMail, MessageSchema, ConnectionConfig
from pydantic import EmailStr
from jinja2 import Environment, select_autoescape, PackageLoader

from app.config import settings
from app.custom_logging import get_logger, log_path

# Get logger
logger = get_logger(
    logger_name=__name__,
    logging_file=log_path
)

# Configure Jinja to load templates
env = Environment(
    loader=PackageLoader('app', 'templates'),
    autoescape=select_autoescape(['html'])
)



class Email:
    """
    Class that sends HTML emails

    Attributes:
        sender: A string indicating the sender of the email
        email: A list of emails addresses to send the email to
        url: A url which will be rendered in the email
    """
    def __init__(self, url: str, email: List[EmailStr]):
        """
        The constructor

        Args:
            url: A url which will be rendered in the mail
            email: A list of email addresses to send the mail to
        """
        self.email = email
        self.url = url
    


    async def sendMail(self, subject: str, template: str):
        """
        Sends an HTML mail

        Args:
            subject: The subject of the email
            template: The name of the template which will be rendered in the email
        """

        # Define the connection configuration
        conf = ConnectionConfig(
            MAIL_USERNAME=settings.EMAIL_USERNAME,
            MAIL_PASSWORD=settings.EMAIL_PASSWORD,
            MAIL_FROM=settings.EMAIL_FROM,
            MAIL_PORT=settings.EMAIL_PORT,
            MAIL_SERVER=settings.EMAIL_HOST,
            MAIL_STARTTLS=True,
            MAIL_SSL_TLS=False,
            USE_CREDENTIALS=True,
            VALIDATE_CERTS=True
        )

        # Generate the HTML template base on the template name
        template = env.get_template(f'{template}.html')

        html = template.render(
            url=self.url,
            subject=subject
        )

        # Define the message options
        message = MessageSchema(
            subject=subject,
            recipients=self.email,
            body=html,
            subtype="html"
        )

        logger.info(f"Sending '{subject}' email(s) to {self.email}")

        # Send the email
        fm = FastMail(conf)
        await fm.send_message(message)