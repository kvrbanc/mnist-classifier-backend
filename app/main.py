from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse
from fastapi.middleware.cors import CORSMiddleware
from fastapi_jwt_auth.exceptions import AuthJWTException

from app.config import settings
from app.router import auth, classifier
from app.custom_logging import get_logger, log_path

# Get logger 
logger = get_logger(
    logger_name=__name__,
    logging_file=log_path
)


app = FastAPI()


origins = [
    settings.CLIENT_ORIGIN_LOCALHOST,
    settings.CLIENT_ORIGIN_CONTAINER
]

# Add CORS policy
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)



# Define routing
app.include_router(auth.router, tags=['Auth'], prefix='/api/auth')
app.include_router(classifier.router, tags=['Classifier'], prefix='/api/classifier')



@app.exception_handler(AuthJWTException)
def authjwt_exception_handler(request: Request, exc: AuthJWTException):
    """
    Handler for the JWT authentication exception 
    """
    logger.info(f"Cought JWT exception. Status: {exc.status_code}, Details: {exc.message}")
    return JSONResponse(
        status_code=exc.status_code,
        content={
            "detail": exc.message
        }
    )



@app.get("/api/")
def root():
    """
    A healthchecker API endpoint
    """
    return {
        "message": "Welcome to the MNIST classifier backend"
    }

