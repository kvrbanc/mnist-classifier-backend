import base64
from fastapi_jwt_auth import AuthJWT
from fastapi import HTTPException, Depends, status
from pydantic import BaseModel
from typing import List
from bson.objectid import ObjectId

from app.config import settings
from app.database import User
from app.serializers.userSerializers import userEntity



class Settings(BaseModel):
    """
    Class that specifies settings for the JWT authentication 
    """
    authjwt_algorithm: str = settings.JWT_ALGORITHM
    authjwt_decode_algorithms: List[str] = [settings.JWT_ALGORITHM]
    authjwt_token_location: set = {'headers'}
    authjwt_header_name: str = 'Authorization'
    authjwt_header_type: str = 'Bearer'
    authjwt_public_key: str = base64.b64decode(
        settings.JWT_PUBLIC_KEY).decode('utf-8')
    authjwt_private_key: str = base64.b64decode(
        settings.JWT_PRIVATE_KEY).decode('utf-8')


# Load the JWT configuration
@AuthJWT.load_config
def get_config():
    return Settings()


# Custom exception classes
class NotVerified(Exception):
    pass

class UserNotFound(Exception):
    pass


def require_user(Authorize: AuthJWT = Depends()) -> str:
    """
    Validates JWT token, and extracts user id from the token

    Args:
        Authorize: An instance of AuthJWT class provided by the means of dependency injection

    Raises:
        HTTPException: The token is missing from the request headers
        HTTPException: The user with the extracted id does not exist
        HTTPException: The user with the extracted id does not yet have a verified account
        HTTPException: Token is invalid or expired

    Returns:
        str: User id extracted from the JWT token
    """

    try:
        # Ensure that the token is valid
        Authorize.jwt_required()
        # Get the subject of the JWT
        user_id = Authorize.get_jwt_subject()
        
        user = userEntity(User.find_one({'_id': ObjectId(str(user_id))}))

        if not user:
            raise UserNotFound('User does not exist.')

        if not user["verified"]:
            raise NotVerified('Your account hasn\'t been verified. Please verify your account by clicking the link in the mail.')

    except Exception as e:
        error = e.__class__.__name__
        if error == 'MissingTokenError':
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED, 
                detail='You are not logged in.'
            )
        if error == 'UserNotFound':
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED, 
                detail='User does not exist.'
            )
        if error == 'NotVerified':
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED, 
                detail='Your account hasn\'t been verified. Please verify your account by clicking the link in the mail.'
            )

        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, 
            detail='Token is invalid or has expired'
        )
    
    return user_id