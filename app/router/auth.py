from random import randbytes
import hashlib

from datetime import datetime, timedelta
from fastapi import APIRouter, status, Depends, HTTPException, Request
from pydantic import EmailStr

from app import oauth2, schemas, utils
from app.database import User
from app.config import settings
from app.serializers.userSerializers import userEntity
from app.email import Email
from app.custom_logging import get_logger, log_path

# Get logger
logger = get_logger(
    logger_name=__name__,
    logging_file=log_path
)


router = APIRouter()
ACCESS_TOKEN_EXPIRES_IN = settings.ACCESS_TOKEN_EXPIRES_IN



@router.post('/register', status_code=status.HTTP_201_CREATED)
async def create_user(payload: schemas.UserRegisterSchema, request: Request):
    """
    Registers a new user, generates a verification token, and sends the token to the user's email address
    """

    # Check if the user already exists
    user = User.find_one({'email': payload.email.lower()})
    if user:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail='Account with that email already exists'
        )
    
    # Compare password and passwordConfirm
    if payload.password != payload.passwordConfirm:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, 
            detail='Passwords do not match'
        )
    
    # Hash the password
    payload.password = utils.hash_password(payload.password)
    created_at = datetime.utcnow()

    # Create an instance of a user that will be saved
    user = schemas.User(
        email=payload.email.lower(),
        password=payload.password,
        created_at=created_at,
        updated_at=created_at,
        verified=False
    )

    # Save the new user
    result = User.insert_one(user.dict())
    
    # Send the verification token to the user's email
    try:
        # Verification token - random ten bytes
        token = randbytes(10)

        # Hash the verification token and save it into database
        hashedCode = hashlib.sha256()
        hashedCode.update(token)
        verification_code = hashedCode.hexdigest()
        User.find_one_and_update(
            {
                "_id": result.inserted_id
            }, 
            {
                "$set": {
                            "verification_code": verification_code, 
                            "updated_at": datetime.utcnow()
                        }
            })
        
        logger.info(f"Successfully created user with email {user.email} and generated a verification code. Sending verification code via email ...")
        
        # Construct link for the user to access and send the mail
        url = f"{settings.EMAIL_LINK_CLIENT_ORIGIN}/verify?token={token.hex()}"
        await Email(url, [EmailStr(payload.email)]).sendMail(subject="Your verification code", template="verification")

    except Exception as error:
        # If there was an error, delete previously saved user
        User.delete_one({
            "_id": result.inserted_id
        })
        
        logger.exception(f'There was an error while registering a new user with email {user.email}')

        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail='There was an error while registering a new user'
        )
    
    return {
        'status': 'successful', 
        'message': 'Verification token has been sent to your email'
    }



@router.get('/verifyemail/{token}')
async def verify_token(token: str):
    """
    Verifies user using the provided verification token
    """

    # Hash the verification token
    hashedCode = hashlib.sha256()
    hashedCode.update(bytes.fromhex(token))
    verification_code = hashedCode.hexdigest()

    # Check if the token is present in the database
    result = User.find_one_and_update(
        {
            "verification_code": verification_code
        }, 
        {
            "$set": {
                        "verification_code": None, 
                        "verified": True, 
                        "updated_at": datetime.utcnow()
                    }
        }, new=True)

    if not result:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, 
            detail='Invalid verification code or account already verified'
        )
    
    return {
        "status": "success",
        "message": "Account verified successfully"
    }



@router.post('/login', response_model=schemas.Token)
async def login(payload: schemas.UserLoginSchema, Authorize: oauth2.AuthJWT = Depends()):
    """
    Checks user's credentials and generates a JWT access token
    """
    # Check if the user exists
    db_user = User.find_one({'email': payload.email.lower()})
    if not db_user:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail='Incorrect email'
        )
    
    user = userEntity(db_user)
    if not user['verified']:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Your account hasn\'t been verified. Please verify your account by clicking on the link in the mail.'
        )

    # Check if the password is valid
    if not utils.verify_password(payload.password, user['password']):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail='Incorrect password'
        )

    # Create access token
    access_token = Authorize.create_access_token(
                    subject=str(user["id"]), 
                    expires_time=timedelta(minutes=ACCESS_TOKEN_EXPIRES_IN)
                )
    logger.info(f"User with email {user['email']} successfully logged in. JWT access token is generated and sent in the response.")
    
    response = schemas.Token(
        access_token=access_token,
        token_type='Bearer'
    )
    
    # Return access token
    return response



@router.get('/password/reset/{email}')
async def generate_password_reset_code(email: EmailStr, request: Request):
    """
    Generates a password reset code and sends it to the provided email address
    """
    # Check if the user exists
    user = User.find_one({'email': email.lower()})
    if not user:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Account does not exist"
        )
    
    # Send the password reset token to users email
    try:
        # Password reset token - random ten bytes
        token = randbytes(10)

        # Hash the reset token and save it into database
        hashedCode = hashlib.sha256()
        hashedCode.update(token)
        reset_code = hashedCode.hexdigest()
        User.find_one_and_update(
            {
                "email": email
            }, 
            {
                "$set": {
                            "password_reset_code": reset_code, 
                            "updated_at": datetime.utcnow()
                        }
            })
        
        logger.info(f"Password reset code is successfully generated for the user with email {email}. Sending the reset code via email...")
        
        # Construct link for the user to access and send the mail
        url = f"{settings.EMAIL_LINK_CLIENT_ORIGIN}/password/reset?token={token.hex()}"
        await Email(url, [EmailStr(email)]).sendMail(subject="Your password reset code", template="passwordReset")

    except Exception as error:
        # If there was an error, delete the previously generated password reset code
        User.find_one_and_update(
        {
            "email": email
        }, 
        {
            "$set": {
                        "password_reset_code": None, 
                        "updated_at": datetime.utcnow()
                    }
        })
        logger.exception(f"There was an error while generating password reset code for a user with email {email}")

        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail='There was an error while generating password reset code'
        )
    
    return {
        'status': 'success', 
        'message': 'Password reset code has been sent to your mail'
    }



@router.post("/password/reset/")
async def reset_password(payload: schemas.PasswordResetRequestSchema):
    """
    Resets the password for a user using the password reset code
    """

    # Chek if the password and passwordConfirm match
    if not (payload.password == payload.passwordConfirm):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Passwords do not match"
        )
    
    # Hash the password reset token
    hashedCode = hashlib.sha256()
    hashedCode.update(bytes.fromhex(payload.token))
    reset_code = hashedCode.hexdigest()

    password = utils.hash_password(payload.password)
    result = User.find_one_and_update(
        {
            "password_reset_code": reset_code
        }, 
        {
            "$set": {
                "password_reset_code": None,
                "password": password,
                "updated_at": datetime.utcnow()
            }
        }
    )

    if not result:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Password reset code is invalid"
        )
    
    return {
        "status": "success",
        "message": "Password reset successfully"
    }