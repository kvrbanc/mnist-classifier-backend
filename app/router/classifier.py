from fastapi import APIRouter, Depends, HTTPException, status
import httpx
import json

from app import schemas, oauth2
from app.config import settings
from app.custom_logging import get_logger, log_path

# Load logger
logger = get_logger(
    logger_name=__name__,
    logging_file=log_path
)

router = APIRouter()
CLASSIFIER_URL = settings.CLASSIFIER_URL



@router.get('/samples', response_model=schemas.DataSamplesResponseSchema)
async def get_mnist_samples(num_samples: int = 4, _ : str = Depends(oauth2.require_user)):
    """
    Retrieves MNIST digit samples from the 'Classifier' service
    """

    url = CLASSIFIER_URL + '/api/samples?num_samples=' + str(num_samples)

    async with httpx.AsyncClient() as client:

        response = await client.get(url)

        # Check status code
        if response.status_code != 200:
            logger.warning(f"GET request to the url: {url} returned status code {response.status_code}. More information: {response.json()}")
            raise HTTPException(
                status_code=response.status_code,
                detail=response.json()
            )
        
        return response.json()



@router.post('/predict', response_model=schemas.PredictLabelResponseSchema)
async def predict_label(payload: schemas.PredictLabelRequestSchema, _ : str = Depends(oauth2.require_user)):
    """
    Sends a request to the 'Classifier' service to predict the digit label
    """

    url = CLASSIFIER_URL + '/api/predict'

    # Check the shape of the given input
    if len(payload.input) != 28: 
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Provided input has the incorrect shape. Expected a list of shape [28,28]."
        )
    for row in payload.input:
        if len(row) != 28:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Provided input has the incorrect shape. Expected a list of shape [28,28]."
            )

    async with httpx.AsyncClient() as client:
        data = {
            "input" : payload.input
        }

        response = await client.post(
            url=url,
            data=json.dumps(data)
        )

        # Check status code
        if response.status_code != 200:
            logger.warning(f"POST request to the url: {url} returned status code {response.status_code}. More information: {response.json()}")
            raise HTTPException(
                status_code=response.status_code,
                detail=response.json()
            )
        
        return response.json()