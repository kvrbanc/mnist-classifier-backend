def userEntity(user) -> dict:
    return {
        "id": str(user["_id"]),
        "email": user["email"],
        "password": user["password"],
        "created_at": user["created_at"],
        "updated_at": user["updated_at"],
        "verified": user["verified"]
    }


def userResponseEntity(user) -> dict:
    return {
        "id": str(user["_id"]),
        "email": user["email"],
        "created_at": user["created_at"],
        "verified": user["verified"]
    }