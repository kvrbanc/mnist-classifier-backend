from passlib.context import CryptContext


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")



def hash_password(password: str) -> str:
    """
    Computes a hash of the password using bcrypt algorithm

    Args:
        password: A password whose hash needs to be computed

    Returns:
        str: Hashed password
    """
    return pwd_context.hash(password)



def verify_password(password: str, hashed_password: str) -> bool:
    """
    Verifies if the password matches the hashed password

    Args:
        password: A password
        hashed_password: A hash of a password

    Returns:
        bool: Whether the password matches the hashed password
    """
    return pwd_context.verify(password, hashed_password)